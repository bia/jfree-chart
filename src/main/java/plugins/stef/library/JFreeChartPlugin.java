package plugins.stef.library;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * JFree Chart library for Icy
 * 
 * @author Stephane Dallongeville
 */
public class JFreeChartPlugin extends Plugin implements PluginLibrary
{
    //
}
